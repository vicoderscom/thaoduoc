var gulp = require('gulp');
var flatten = require('gulp-font');

module.exports = function() {
    return src('./bower_components/**/fonts/*.{ttf,otf}', { read: false })
        .pipe(gulpFont({
            ext: '.css',
            fontface: './bower_components/**/fonts',
            relative: '/fonts',
            dest: 'dist/fonts',
            embed: ['woff'],
            collate: false
        }))
        .pipe(dest('dist/fonts'));
};