<?php
/**
 * Template Name: Checkout Page
 */
get_header();
?>
<div class="CheckoutPage bg_wraper">
	<div class="container">
		
		<?php
		if (have_posts()):
		    while (have_posts()): the_post();
		        ?>
					<div class="row title-checkout-page">
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 content-title">
							<h3>THÔNG TIN KHÁCH HÀNG</h3>
						</div>
					</div>
					<div class="row content-page" id="post-<?php the_ID();?>">
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 form-container">
							<?php the_content();?>
						</div>
					</div>
				<?php
		    endwhile;
		endif;
		?>
	</div>
</div>
<?php
get_footer();
?>