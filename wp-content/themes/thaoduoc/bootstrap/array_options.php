<?php
// function script_admin() {
// 	wp_enqueue_script('jquery-js', get_stylesheet_directory_uri() . "/bower_components/jquery/dist/jquery.min.js");
// 	wp_enqueue_script('load_select2', get_stylesheet_directory_uri() . "/bower_components/select2/dist/js/select2.min.js");
// }

// add_action('admin_enqueue_scripts', 'script_admin');

function wdm_get_tags($products_array) {
	$numItems = count($products_array);
	foreach ($products_array as $key => $single_product) {
		$list_tag = get_the_term_list($single_product, 'product_tag', '', ' ');
		if (!empty($list_tag)) {
			if ($key == 0) {
				echo "TỪ KHÓA: ";
			}
			echo $list_tag;
			if ($key < ($numItems - 1)) {
				echo ' ';
			}
		}
	}

}


function add_theme_option() {
	add_menu_page(
		'theme_option',
		'Theme option',
		'manage_options',
		'theme_option',
		'show_theme_option',
		'',
		'2'
	);
};

function show_theme_option() {
	// add_option('phone', 'haha');
	// add_option('bottomad', 'haha');
	if(isset($_POST['save_theme_option'])) {
		update_option('phone', $_POST['phone']);
		update_option('bottomad', $_POST['bottomad']);
		update_option('quangcao', $_POST['quangcao']);
	}
	$get_phone = get_option('phone');
	$get_bottomad = get_option('bottomad');
	$get_quangcao = get_option('quangcao');
	include(TEMPLATEPATH . "/bootstrap/theme_option.php");
}
add_action('admin_menu', 'add_theme_option');


add_filter( 'get_comment_date', 'wpsites_change_comment_date_format' );	
function wpsites_change_comment_date_format( $d ) {
    $d = date("m.d.y");	
    return $d;
}