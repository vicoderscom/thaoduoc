<?php
/**
* Class for woocommerce
*/
class wc
{
	
	public function __construct()
	{
		add_filter('woocommerce_checkout_fields' , [$this,'billing_field']);
		// add_filter('woocommerce_button_back_checkout' , [$this,'ButtonBackCheckout']);
		add_filter('woocommerce_order_button_text', [$this, 'changeOrderButtonText']);
		remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
		add_filter('woocommerce_cart_needs_payment', [$this, 'removePayment']);
		add_filter('woocommerce_thankyou_order_received_text', [$this, 'replaceTextThankyouOrder']);
	}

	public function billing_field($fields) {
		unset($fields['billing']['billing_country']);
		unset($fields['billing']['billing_company']);
		unset($fields['billing']['billing_city']);
		unset($fields['billing']['billing_postcode']);
		unset($fields['billing']['billing_last_name']);
		unset($fields['billing']['billing_address_2']);
		unset($fields['billing']['billing_state']);

		$fields['billing']['billing_first_name']['label'] = __('Họ tên khách hàng');
		$fields['billing']['billing_first_name']['class'] = ['form-row-third'];

		$fields['billing']['billing_address_1']['label'] = __('Địa chỉ nhận hàng');
		$fields['billing']['billing_address_1']['class'] = ['form-row-half'];
		$fields['billing']['billing_address_1']['type'] = 'textarea';
		$fields['billing']['billing_address_1']['placeholder'] = __('');

		$fields['billing']['billing_phone']['label'] = __('Số điện thoại');

		$emailElem = [
			'billing_email' => [
				'label' => __('Email', 'thaoduoc'),
				'required' => true,
				'type' => 'email',
				'class' => 'form-control',
				'validate' => ['emai'],
				'autocomplete' => 'email username',
				'priority' => 110
			]
		];

		$phoneElem = [
			'billing_phone' => [
				'label' => __('Số điện thoại', 'thaoduoc'),
				'required' => true,
				'type' => 'tel',
				'class' => 'form-control',
				'validate' => ['phone'],
				'autocomplete' => 'tel',
				'priority' => 100
			]
		];

		arrayInsert($fields['billing'], 'billing_address_1', $emailElem);
		arrayInsert($fields['billing'], 'billing_address_1', $phoneElem);

		$fields['billing']['billing_email']['label'] = __('Email');
		$fields['billing']['billing_email']['class'] = ['form-row-third'];
		$fields['billing']['billing_phone']['class'] = ['form-row-third'];
		$fields['order']['order_comments']['label'] = __('Ghi chú chi tiết');
		$fields['order']['order_comments']['type'] = __('textarea');
		$fields['order']['order_comments']['placeholder'] = '';
		$fields['order']['order_comments']['class'] = ['form-row-half'];

		return $fields;
	}

	public function ButtonBackCheckout() {
		return '<a href="'. esc_url( wc_get_cart_url() ) .'"><button type="button">Quay lại</button></a>';
	}

	public function changeOrderButtonText()
	{
		return __('hoàn tất', 'thaoduoc');
	}

	public function removePayment() {
		return false;
	}

	public function replaceTextThankyouOrder(){
		return 'Cảm ơn bạn. Đơn hàng của bạn đã được gửi đi !';
	}
}

new wc();