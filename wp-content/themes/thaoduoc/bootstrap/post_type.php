<?php
function register_news_taxonomies() {
	register_taxonomy('category_news', 'news', array(
		'label' => 'Categories News',
		'hierarchical' => true,
	));
}

function register_handbook_taxonomies() {
	register_taxonomy('category_handbook', 'handbook', array(
		'label' => 'Categories Handbook',
		'hierarchical' => true,
	));
}

add_action('init', 'register_news_taxonomies');
add_action('init', 'register_handbook_taxonomies');
