	<div class="button_scroll_top">
	    <i id="button_scroll_top">
			<span class="fa fa-chevron-up" aria-hidden="true"></span>
		</i>
	</div>
	<div class="container-fluid pad-l-0 pad-r-0 footer-section" id="footer_site">
		<?php echo view('layouts.footer'); ?>
	</div>
	<?php wp_footer(); ?>
</body>
</html>

<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>