<?php

namespace App\CustomPostType;

use App\CustomPostType\Core\CustomPostType;

/**
 * News post type
 */
class NewsPostType extends CustomPostType
{

    public function __construct()
    {
        $fields = [
            'label'              => 'News Category',
            'singular_label'     => 'News',
            'slug_taxonomy'      => 'news', 
            'slug_post_taxonomy' => 'chi-tiet-tin-tuc', // to show on detail post type on url
            'alias_taxonomy'     => 'news', // admin: edit?post_type=news
            'name_taxonomy'      => 'product_tag',
        ];
        parent::__construct($fields);
    }
}
