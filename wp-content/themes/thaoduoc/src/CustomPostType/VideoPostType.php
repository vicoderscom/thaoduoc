<?php

namespace App\CustomPostType;

use App\CustomPostType\Core\CustomPostType;

/**
 * Hanbook post type
 */
class VideoPostType extends CustomPostType
{

    public function __construct()
    {
        $fields = [
            'label'              => 'Video',
            'singular_label'     => 'Video',
            'slug_taxonomy'      => 'video', 
            'slug_post_taxonomy' => 'chi-tiet-video', // to show on detail post type on url
            'alias_taxonomy'     => 'video', // admin?post_type=category_handbook
            'name_taxonomy'      => 'video_tag',
        ];
        parent::__construct($fields);
    }
}
