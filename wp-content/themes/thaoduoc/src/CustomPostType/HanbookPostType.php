<?php

namespace App\CustomPostType;

use App\CustomPostType\Core\CustomPostType;

/**
 * Hanbook post type
 */
class HanbookPostType extends CustomPostType
{

    public function __construct()
    {
        $fields = [
            'label'              => 'Handbook Category',
            'singular_label'     => 'Handbook',
            'slug_taxonomy'      => 'handbook', 
            'slug_post_taxonomy' => 'chi-tiet-cam-nang', // to show on detail post type on url
            'alias_taxonomy'     => 'handbook', // admin?post_type=category_handbook
            'name_taxonomy'      => 'product_tag',
        ];
        parent::__construct($fields);
    }
}
