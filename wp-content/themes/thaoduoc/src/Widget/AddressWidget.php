<?php 

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class AddressWidget extends Widget
{
	public function __construct()
	{
		$widget = [
		    'id'          => 'address_widget',
		    'label'       => __('Address Widget', 'thaoduoc'),
		    'description' => 'This widget shows address information'
		];

		$fields = [
			[
		        'label' => __('Icon address', 'thaoduoc'),
		        'name'  => 'icon-address',
		        'type'  => 'text',
			],
			[
		        'label' => __('Address', 'thaoduoc'),
		        'name'  => 'address',
		        'type'  => 'text',
			]
		];


		parent::__construct($widget, $fields);
	}

	public function handle($instance) {
		?>
		<style type="text/css">
			.address-container .icon-container {
				float: left;
				margin-right: 10px;
			}
			.address-container .icon-container .fa{
				font-size: 20px;
			}
			.address-container .address-content {
				font-size: 16px;
			}
		</style>
		<span class="address-container">
			<div class="icon-container"><i class="" aria-hidden="true"><img src="<?php echo $instance['icon-address'] ?>" alt=""></i></div>
			<p class="address-content"><?php echo $instance['address']; ?></p>
		</span>
		<?php
	}
}