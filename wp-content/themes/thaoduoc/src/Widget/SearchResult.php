<?php

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class SearchResult extends Widget {
	public function __construct() {
		$widget = [
			'id'          => 'searchresult',
			'label'       => __('SearchResult', 'searchresult'),
			'description' => 'This widget shows Search result information',
		];

		$fields = [
			[
                'label' => __('ID các category', 'thaoduoc'),
                'name'  => 'IDs',
                'type'  => 'text',
            ],
            [
                'label' => __('Text link more', 'thaoduoc'),
                'name'  => 'text_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Icon link mở rộng', 'thaoduoc'),
                'name'  => 'icon_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Số cột chia sản phẩm (max: 12)', 'thaoduoc'),
                'name'  => 'number_column',
                'type'  => 'text',
            ]
		];
		parent::__construct($widget, $fields);

	}

    function filter_where_title($where = '') {
        $query_string = get_search_query();
        $where .= " OR post_title LIKE '%".$query_string."%'";                   
        return $where;
    }

	public function handle($instance) {
		global $post, $wp_query, $product;
        ?>
        <div class="homepage-product-list container">
            <div class="product-cate-box">
            <div class="row product-cate-list">
                
                <?php
                if(isset($_GET['s'])) {
                    $keysearch = esc_attr($_GET['s']);
                    if(empty($keysearch)) {
                        echo "<p class='container-fluid'>không tìm thấy kết quả. Vui lòng xem lại từ khoá tìm kiếm</p>";
                    } else {
                        echo "<p class='container-fluid' style='clear:left;'>Kết quả tìm kiếm của từ khóa: '$keysearch'</p>";
                        $query_string = get_search_query();

                        $paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

                        $args = array(
                                // 'post_type' => array("product", "handbook", 'news'),
                                'post_type' => array("product", "handbook"),
                                // 'post_type' => 'product',
                                'meta_query' => array(
                                    'relation' => 'OR',
                                    array(
                                            'key' => 'tab_retail',
                                        'value' => $query_string,
                                        'compare' => 'LIKE'
                                    ),
                                    array(
                                        'key' => 'tab_tech',
                                        'value' => $query_string,
                                        'compare' => 'LIKE'
                                    ),
                                    array(
                                        'key' => 'tab_shipping_information',
                                        'value' => $query_string,
                                        'compare' => 'LIKE'
                                    )
                                ),                      
                                'posts_per_page' => 12,
                                'paged' => $paged                     
                        );
                        $this->filter_where_title();
                        $get_product = new \WP_Query();
                        add_filter("posts_where", [$this, "filter_where_title"]);
                        $search_posts = $get_product->query($args);  
                        foreach($search_posts as $val_search) {
                            // if($val_search->post_type == 'product' || $val_search->post_type == 'news' || $val_search->post_type == 'handbook') {
                            $id = $val_search->ID;
                            $title = $val_search->post_title;
                            $comment_count = $val_search->comment_count;
                            $img = wp_get_attachment_url(get_post_thumbnail_id($id));
                            $img = ($img) ? $img : 'http://placehold.it/128x164';
                            $url = get_permalink($val_search->ID);
                            $check_post_type = get_post_type($id);
                            if($check_post_type === 'product') {
                                $product = wc_get_product($id);

                                $price = $product->price;
                            }


                            $data = [
                                    'id' => $id,
                                    'title' => $title,
                                    'img'   => $img,
                                    'url'   => $url
                                ];

                            $col = $instance['number_column'];
                            if(empty($col)) {
                                $col = 3;
                            }
                            ?>
                            <div class="col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12 product-column">
                                <div class="product-item">
                                        <div class="product-cate-img">
                                            <a href="<?php echo $url; ?>">
                                                <img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url(<?php echo $img; ?>) no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;" />
                                            </a>
                                        </div>
                                        <div class="product-cate-title">
                                            <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
                                        </div>
                                        <?php if($check_post_type === 'product'){ ?>
                                            <div class="product-price">
                                                <span class="title-price">Giá bán: </span>
                                                <span class="price-main">
                                                    <?php 
                                                        echo WC_price($price);
                                                    ?>
                                                </span>
                                            </div>
                                        <?php }else{ ?>

                                            <div class="no_product" style="height: 25px;">
                                    
                                            </div>

                                        <?php } ?>
                                        <div class="row more-info">
                                            <div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
                                                <i class="fa fa-calendar" aria-hidden="true"></i> 
                                                <span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $val_search->ID ); ?></span>
                                            </div>
                                            <!-- <div class="col-md-6 col-sm-6 col-xs-6 pull-right sub-more-right">
                                                <i class="fa fa-comments" aria-hidden="true"></i>
                                                <span class="comment-post">
                                                    <?php echo $comment_count; ?> phản hồi
                                                </span>
                                            </div> -->
                                        </div>
                                </div>
                            </div>
                            <?php
                            // }
                        }
                        echo '<div class="contain_pagination">';
                            echo '<div class="paginate pull-right">';
                            $total_pages = $get_product->max_num_pages;

                            // var_dump($total_pages);

                            if ($total_pages > 1) {

                                $current_page = max(1, $paged);

                                echo paginate_links(array(
                                    'base' => @add_query_arg('trang','%#%'),
                                    'format' => '?trang=%#%',
                                    'current' => $current_page,
                                    'total' => $total_pages,
                                        'prev_text'    => __('<'),
                                        'next_text'    => __('>')
                                ));
                            }
                            wp_reset_postdata();
                            echo '</div>';
                        echo '</div>';
                        // remove_filter('posts_where', 'filter_where_title');
                    }
                } else {
                    echo "Không tồn tại từ khoá.";
                }
                ?>
        </div>
    </div>
</div>
    <?php
	}
}
