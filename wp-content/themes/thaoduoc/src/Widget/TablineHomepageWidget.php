<?php

namespace App\Widget;

use MSC\Widget;
/**
* 
*/
class TablineHomepageWidget extends Widget
{
	
	public function __construct()
	{
		$widget = [
            'id'          => 'tabline_homepage',
            'label'       => __('Tab line on homepage', 'thaoduoc'),
            'description' => 'This widget shows tab line on homepage',
        ];

        $fields = [
            [
                'label' => __('Title', 'thaoduoc'),
                'name'  => 'text-show',
                'type'  => 'text',
            ]
        ];

        parent::__construct($widget, $fields);
	}

	public function handle($instance) {
		?>
		<style type="text/css">
			.tabline {
				padding-left: 0;
    			padding-right: 0;
    			height: 45px;
			    background-repeat: no-repeat;
			    background-position: center;
			    background-size: 98%;
		        position: relative;
		        font-family: segoe;
		        margin-top: 60px;
			}
			.tabline .text-show {
				position: absolute;
				color: #fff;
			    font-size: 20px;
			    line-height: 45px;
			    text-align: center;
			    width: 100%;
			    text-transform: uppercase;
			}
		</style>
		<?php
	}
}