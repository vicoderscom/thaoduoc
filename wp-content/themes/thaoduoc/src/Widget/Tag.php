<?php

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class Tag extends Widget {
	public function __construct() {
		$widget = [
			'id'          => 'tag',
			'label'       => __('Tag', 'tag'),
			'description' => 'This widget shows address information',
		];

		$fields = [
			[
                'label' => __('ID các category', 'thaoduoc'),
                'name'  => 'IDs',
                'type'  => 'text',
            ],
            [
                'label' => __('Text link more', 'thaoduoc'),
                'name'  => 'text_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Icon link mở rộng', 'thaoduoc'),
                'name'  => 'icon_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Số cột chia sản phẩm (max: 12)', 'thaoduoc'),
                'name'  => 'number_column',
                'type'  => 'text',
            ]
		];
		parent::__construct($widget, $fields);
	}

	public function handle($instance) {
		global $post, $wp_query, $product;
        $cat_title = get_queried_object(); 
        $cat_slug = $cat_title->slug; 
        if($cat_title->taxonomy == 'post_tag') {
            $taxo_tag = 'post_tag';
            $type_post = 'news';
        } else {
            $taxo_tag ='product_tag';
            $type_post = 'product';
        }
        ?>
        <div class="homepage-product-list container">
            <div class="product-cate-box">
                <?php 
                
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $array_product = [
                    'post_type'             => ['product', 'news', 'handbook'],
                    'post_status'           => 'publish',
                    'order'=> 'ASC',
                    'tax_query' => [
                                       [
                                          'taxonomy' =>$taxo_tag,
                                          'field' => 'slug',
                                          'terms' =>$cat_slug
                                       ]
                                   ],
                    'paged' => $paged
                ];
                $get_products = new \WP_Query($array_product);  

                $max_num_pages = $get_products->max_num_pages;
                if($get_products->have_posts()) {
                    echo '<div class="row product-cate-list">';

                    foreach ($get_products->posts as $key => $pro) {
                        $id = $pro->ID;
                        $title = $pro->post_title;
                        $comment_count = $pro->comment_count;
                        $img = wp_get_attachment_url(get_post_thumbnail_id($id));
                        $img = ($img) ? $img : 'http://placehold.it/128x164';
                        $url = get_permalink($pro->ID);

                        $data = [
                                'id' => $id,
                                'title' => $title,
                                'img'   => $img,
                                'url'   => $url
                            ];

                        $col = $instance['number_column'];
                        if(empty($col)) {
                        	$col = 3;
                        }
                        if($pro->post_type === 'product'){
                            $product = new \WC_Product($pro->ID);
                            $price = $product->get_price();
                        }
                        ?>
                        <div class="col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12 product-column">
                            <div class="product-item">
                                <div class="product-cate-img">
                            		<a href="<?php echo $url; ?>">
                                    	<img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;" />
                                	</a>
                                </div>
                                <div class="product-cate-title">
                                    <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
                                </div>
                                <?php if($pro->post_type === 'product'){ ?>
                                <div class="product-price">
                                    <span class="title-price"><?php echo $price ? 'Giá bán:' : null; ?></span>
                                    <span class="price-main"><?php echo $price ? wc_price($price) : null; ?></span>
                                </div>
                                <?php } else { ?>
                                <div class="product-price">
                                    <span class="title-price" style="visibility: hidden;">.</span>
                                    <span class="price-main"></span>
                                </div>
                                <?php } ?>
                               	<div class="row more-info">
                               		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
                               			<i class="fa fa-calendar" aria-hidden="true"></i> 
                               			<span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $pro->ID ); ?></span>
                               		</div>
                               		<div class="col-md-6 col-sm-6 col-xs-6 pull-right sub-more-right">
                               			<i class="fa fa-comments" aria-hidden="true"></i>
                               			<span class="comment-post">
                               				<?php echo $comment_count; ?> phản hồi
                               			</span>
                               		</div>
                               	</div>
                            </div>
                        </div>
                        <?php
                    }
                    echo '</div>';
                }
                do_action('custom_paginate', $max_num_pages);
                ?>
            </div>
        </div>
    <?php
	}
}
