<?php

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class ProductSameTopic extends Widget {
	public function __construct() {
		$widget = [
			'id'          => 'product_same_topic',
			'label'       => __('Product Same Topic', 'product_same_topic'),
			'description' => 'This widget shows Product Same Topic information',
		];

		$fields = [
			[
				'label' => __('Product Same Topic', 'product_same_topic'),
				'name'  => 'icon-address',
				'type'  => 'text',
			],
			[
				'label' => __('Product Same Topic', 'product_same_topic'),
				'name'  => 'address',
				'type'  => 'text',
			],
		];

		parent::__construct($widget, $fields);
	}

	public function handle($instance) {
		global $post, $wp_query, $product;
		
		$post_current_id = get_queried_object()->ID; 
		$categories = get_the_terms($post_current_id, 'product_cat');
		$post_same_topic = get_field('bai_viet_cung_chu_de');
		?>
		<div class="waper_product_same_topic waper_product_involve bg_detai">
			<div class="product_involve">
				<h1>Bài viết cùng chủ để</h1>
			</div>
			<div class="list_item">
				<?php  
				if(is_array($post_same_topic) && count($post_same_topic) > 0) {
					foreach($post_same_topic as $topic_val) {
						$image = wp_get_attachment_url(get_post_thumbnail_id($topic_val->ID));
						$link = get_permalink($topic_val->ID);
						$no_image = get_template_directory_uri() . '/assets/images/no_image_1.jpg';
				    ?>
		            <div class="item_list">
		                <div class="col-md-12">
		                    <div class="row">
				                <div class="col-md-5 col-sm-12">
				                    <div class="row">
				                        <a href="<?php echo $link; ?>">
							            	<div class="img bg_img" style="background-image: url(<?php echo $image ? $image : $no_image; ?>)">
							            		<img class="item_img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/detail/trans.png" alt="">
							            	</div>
						            	</a>
						            </div>
					            </div>
				            	<div class="col-md-7 col-sm-12">
					            	<div class="info">
					            		<h1 class="title">
					            		    <a href="<?php echo $link; ?>">
						            		   <?php echo $topic_val->post_title; ?>
					            		    </a>
					            		</h1>
					            	</div>
					            </div>
		                    </div>
		                </div>
		            </div>
				    <?php
					}
				} else {
					echo "<p class='no_product_sidebar'>Không có sản phẩm cùng chủ đề</p>";
				}
				?>
			</div>
		</div>
	<?php
	}
}