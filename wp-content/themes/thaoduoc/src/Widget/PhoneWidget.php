<?php 

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about phone
 */
class PhoneWidget extends Widget
{
	public function __construct()
	{
		$widget = [
		    'id'          => 'phone_widget',
		    'label'       => __('Phone Widget', 'thaoduoc'),
		    'description' => 'This widget shows phone information'
		];

		$fields = [
			[
		        'label' => __('Icon phone', 'thaoduoc'),
		        'name'  => 'icon-phone',
		        'type'  => 'text',
			],
			[
		        'label' => __('phone', 'thaoduoc'),
		        'name'  => 'phone',
		        'type'  => 'text',
			]
		];


		parent::__construct($widget, $fields);
	}

	public function handle($instance) {
		?>
		<style type="text/css">
			.phone-container .icon-container {
				float: left;
				margin-right: 10px;
			}
			.phone-container .icon-container .fa{
				font-size: 20px;
			}
			.phone-container .phone-content {
				font-size: 16px;
			}
		</style>
		<span class="phone-container">
			<div class="icon-container"><i class="fa <?php echo $instance['icon-phone'] ?>" aria-hidden="true"></i></div>
			<p class="phone-content"><?php echo $instance['phone']; ?></p>
		</span>
		<?php
	}
}