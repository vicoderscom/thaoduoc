<?php

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class ProductInvolve extends Widget {
	public function __construct() {
		$widget = [
			'id'          => 'product_involve',
			'label'       => __('Product Involve', 'product_involve'),
			'description' => 'This widget shows address information',
		];

		$fields = [
			[
				'label' => __('Product Involve', 'product_involve'),
				'name'  => 'icon-address',
				'type'  => 'text',
			],
			[
				'label' => __('involve', 'involve'),
				'name'  => 'address',
				'type'  => 'text',
			],
		];

		parent::__construct($widget, $fields);

	}

	public function handle($instance) {
		global $post, $wp_query, $product;
		$arr_product = [
			'post_type'      => 'product',
			'posts_per_page' => 20,
			'post_status' => 'publish'
		];
		?>
		<?php 
		    $meta_data = get_field('bai_viet_lien_quan');
		    $post_current_id = get_queried_object()->ID; 
		 ?>
		<div class="waper_product_involve bg_detai">
			<div class="product_involve">
				<h1>Sản phẩm liên quan</h1>
			</div>
			<div class="list_item">
				<?php
					if(is_array($meta_data) && count($meta_data) > 0) {
					    foreach($meta_data as $data_val) {
					    	$image = wp_get_attachment_url(get_post_thumbnail_id($data_val->ID));
					    	$link = get_permalink($data_val->ID);
					    	$product = new \WC_Product($data_val->ID);
					    	$price = $product->get_price();
					    	$no_image = get_template_directory_uri() . '/assets/images/no_image_1.jpg';
					    	if(!($data_val->ID == $post_current_id)) {
					    ?>
			            <div class="item_list">
			                <div class="col-md-12">
			                    <div class="row">
					                <div class="col-md-5 col-sm-12">
					                    <div class="row">
					                        <a href="<?php echo $link; ?>">
								            	<div class="img bg_img" style="background-image: url(<?php echo $image ? $image : $no_image; ?>)">
								            		<img class="item_img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/detail/trans.png" alt="">
								            	</div>
								            </a>
							            </div>
						            </div>
					            	<div class="col-md-7 col-sm-12">
						            	<div class="info">
						            		<h1 class="title">
						            		    <a href="<?php echo $link; ?>">
						            		        <?php echo $data_val->post_title; ?>
						            		    </a>
						            		</h1>
						            		<p class="price">Giá bán: <strong><?php echo WC_price($price); ?></strong></p>
						            	</div>
						            </div>
			                    </div>
			                </div>
			            </div>
					    <?php
					        }
					    }
					} else {
						echo "<p class='no_product_sidebar'>Không có sản phẩm liên quan</p>";
					}
			    ?>
			</div>
		</div>
    <?php
	}
}
