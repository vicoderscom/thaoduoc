<?php 

namespace App\Widget;

use MSC\Widget;

/**
* Show title as a line on homepage
*/
class QuangCaoHome extends Widget
{
	public function __construct()
    {
        $widget = [
            'id'          => 'quangcao',
            'label'       => __('Quảng cáo và form trang chủ', 'thaoduoc'),
            'description' => 'This widget shows product by category',
        ];

        $fields = [
            [
                'label' => __('Ảnh quảng cáo', 'thaoduoc'),
                'name'  => 'images',
                'type'  => 'upload',
            ],
            [
                'label' => __('Shortcode Form', 'thaoduoc'),
                'name'  => 'shortcode',
                'type'  => 'text',
            ],
        ];

        parent::__construct($widget, $fields);
    }

    /**
     * [handle description]
     * @param  [type] $instance [description]
     * @return [type]           [description]
     */
    public function handle($instance)
    {
        ?>
            <div class="quangcao">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 images">
                            <img style="background: url(<?php echo $instance['images'] ?>); ?>);" src="<?php echo asset('images/mac_dinh.png'); ?>">
                        </div>

                        <div class="col-md-6 form_dangky">
                            <div class="contact">
                                <?php
                                    echo do_shortcode($instance["shortcode"]);
                                ?>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        <?php
    }
}