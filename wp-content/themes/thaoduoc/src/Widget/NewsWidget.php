<?php

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class NewsWidget extends Widget {
	public function __construct() {
		$widget = [
			'id'          => 'news',
			'label'       => __('News', 'news'),
			'description' => 'This widget shows address information',
		];

		$fields = [
			[
                'label' => __('ID các category', 'thaoduoc'),
                'name'  => 'IDs',
                'type'  => 'text',
            ],
            [
                'label' => __('Text link more', 'thaoduoc'),
                'name'  => 'text_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Icon link mở rộng', 'thaoduoc'),
                'name'  => 'icon_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Số cột chia sản phẩm (max: 12)', 'thaoduoc'),
                'name'  => 'number_column',
                'type'  => 'text',
            ]
		];
		parent::__construct($widget, $fields);

	}

	public function handle($instance) {
		global $post, $wp_query, $product;
        $cat_title = get_queried_object(); 
            if(isset($cat_title->taxonomy))  {
                $cat_slug = $cat_title->slug;
                $title = $cat_title->name;
            } else {
                $title = $cat_title->post_title;
        }
        ?>
        <div class="homepage-product-list container">
            <div class="product-cate-box">
                <?php 
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args_pro = [
                    'post_type'             => 'news',
                    'post_status'           => 'publish',
                    'tax_query' => [
                                       [
                                          'taxonomy' =>'category_news',
                                          'field' => 'slug',
                                          'terms' =>$cat_slug
                                       ]
                                   ],
                    'posts_per_page'        => 12,
                    'paged' => $paged
                ];
                if(!isset($cat_title->taxonomy)) {
                    unset($args_pro['tax_query']);
                }
                $get_products = new \WP_Query($args_pro);
                $max_num_pages = $get_products->max_num_pages;
                if (!empty($get_products)) {
                    echo '<div class="row product-cate-list">';

                    foreach ($get_products->posts as $key => $pro) {
                        $id = $pro->ID;
                        $title = $pro->post_title;
                        $comment_count = $pro->comment_count;
                        $img = wp_get_attachment_url(get_post_thumbnail_id($id));
                        $img = ($img) ? $img : 'http://placehold.it/128x164';
                        $url = get_permalink($pro->ID);

                        $data = [
                                'id' => $id,
                                'title' => $title,
                                'img'   => $img,
                                'url'   => $url
                            ];

                        $col = $instance['number_column'];
                        if(empty($col)) {
                        	$col = 3;
                        }
                        ?>
                        <div class="col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12 product-column">
                            <div class="product-item">
                                    <div class="product-cate-img">
                                		<a href="<?php echo $url; ?>">
	                                    	<img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;" />
	                                	</a>
	                                </div>
                                    <div class="product-cate-title">
                                        <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
                                    </div>
                                   	<div class="row more-info">
                                   		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
                                   			<i class="fa fa-calendar" aria-hidden="true"></i> 
                                   			<span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $pro->ID ); ?></span>
                                   		</div>
                                   		<!-- <div class="col-md-6 col-sm-6 col-xs-6 pull-right sub-more-right">
                                            <span class="fb-comments-count" data-href="{{ $url }}" fb-xfbml-state="rendered"><span class="fb_comments_count"></span></span> phản hồi
                                   		</div> -->
                                   	</div>
                            </div>
                        </div>
                        <?php
                    }
                    echo '</div>';
                }
                do_action('custom_paginate', $max_num_pages);
                ?>
            </div>
        </div>
    <?php
	}
}
