<?php 

namespace App\Widget;

use MSC\Widget;

/**
 * AddressWidget - Show information of user about address
 */
class VideoSectionWidget extends Widget
{
	public function __construct()
	{
		$widget = [
		    'id'          => 'video_widget',
		    'label'       => __('Video Section Widget', 'thaoduoc'),
		    'description' => 'This widget shows video on homepage'
		];

		$fields = [
			[
		        'label' => __('ID Channel', 'thaoduoc'),
		        'name'  => 'id_channel',
		        'type'  => 'text',
			]
		];


		parent::__construct($widget, $fields);
	}

	public function handle($instance) {
		?>
		<style type="text/css">
			
		</style>
		<?php 
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args_video = [
	        'post_type'             => 'video',
	        'post_status'           => 'publish',
	        'posts_per_page'        => 9,
	        'orderby' => 'title',
	        'paged' => $paged
	    ];
	    $get_video = new \WP_Query($args_video);

		if(!empty($get_video->posts)):
		?>
		<div class="container video-section-homepage">
			<div class="yout-channel">
				<div class="row">
					<div class="col-sm-3 col-xs-12 img_channel">
						<img src="<?php echo get_template_directory_uri() . '/assets/images/youtube.png'; ?>" alt="" width="180" heigth="50">
					</div>
					<div class="col-sm-4 col-xs-12 link_channel">
						<?php if(!empty($instance['id_channel'])): ?>
							<script src="https://apis.google.com/js/platform.js"></script>
							<div class="g-ytsubscribe" data-channelid="<?php echo $instance['id_channel']; ?>" data-layout="full" data-count="default"></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="row slick_video">
				<?php 
				$dem = 0;
				foreach ($get_video->posts as $key => $item) :
					if($dem > 18) {
						break;
					}
					$id_youtube = explode("=", $item->post_content)[1];
					if(!empty($id_youtube)):
				?>
					<div class="col-sm-4 col-xs-12">
						<iframe width="100%" height="300" src="https://www.youtube.com/embed/<?php echo $id_youtube; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
				<?php
					endif;
					$dem++;
				endforeach;
				?>
			</div>
		</div>
		<?php
		endif;
	}
}