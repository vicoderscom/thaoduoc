<?php 
function data_ajax() {
    $ppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 12;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
    $offset = (isset($_POST['offset'])) ? $_POST['offset'] : 0;
    $args_pro = [
        'post_type'             => 'product',
        'post_status'           => 'publish',
        'posts_per_page'        => $ppp,
        'orderby' => 'title',
        'offset'=>$offset,
        'paged' => $paged
    ];
    $get_products = new \WP_Query($args_pro);
    $max_num_pages = $get_products->max_num_pages;
    if($get_products->have_posts()) {
    	echo '<div class="homepage-product-list container">';
	    	echo '<div class="product-cate-box">';
		        echo '<div class="product-cate-list">';

		        foreach ($get_products->posts as $key => $pro) {
		            $id = $pro->ID;
		            $title = $pro->post_title;
		            $img = wp_get_attachment_url(get_post_thumbnail_id($id));
		            $img = ($img) ? $img : 'http://placehold.it/370x235';
		            $url = get_permalink($pro->ID);
		            $comment_count = $pro->comment_count;

		            $data = [
		                    'id' => $id,
		                    'title' => $title,
		                    'img'   => $img,
		                    'url'   => $url
		                ];

		            $col = $instance['number_column'];
		            if(empty($col)) {
		            	$col = 3;
		            }
		            $post_type = $pro->post_type;

		            if($post_type == 'product') {
		                $product = new \WC_Product($pro->ID);
		                $price = $product->get_price();
		            }
		            ?>
		            <div class="col-md-4 col-sm-4 col-xs-12 product-column">
		                <div class="product-item">
		                    <div class="product-cate-img">
		                		<a href="<?php echo $url; ?>">
		                        	<img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;" />
		                    	</a>
		                    </div>
		                    <div class="product-cate-title" style="<?php echo ($post_type != 'product') ? 'margin-bottom: 0;' : ''; ?>">
		                        <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
		                    </div>
		                   <div class="product-price">
	                            <?php  
	                            if($post_type == 'product'):
	                            ?>
	                            <span class="title-price">Giá bán: </span>
	                            <span class="price-main"><?php echo wc_price($price); ?></span>
	                            <?php else: ?>
	                            <span class="title-price" style="opacity:0;">Loại bài viết</span>
	                            <?php endif; ?>
	                        </div>
		                   	<div class="row more-info">
		                   		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
		                   			<i class="fa fa-calendar" aria-hidden="true"></i> 
		                   			<span class="datetime-post">
		                                <?php echo get_the_date( 'd/m/Y', $pro->ID ); ?>
		                            </span>
		                   		</div>
		                   		<div class="col-md-6 col-sm-6 col-xs-6 text-right count_buy_wrap">
		                   			<!-- <i class="fa fa-comments" aria-hidden="true"></i>
		                   			<span class="comment-post">
		                   				<?php //echo $comment_count; ?> phản hồi
		                   			</span> -->
		                   			<!-- <span class="fa fa-user"></span>  -->
		                   			<span class="p-view-count"><?php echo show_count_price_product($pro->ID); ?></span> người đã mua
		                   		</div>
		                   	</div>
		                </div>
		            </div>
		            <?php
		        }
		        echo '</div>';
	        echo '</div>';
        echo '</div>';
    } else {
    	echo "<script>alert('Đã hết sản phẩm.');</script>";
    }
 wp_die(); } 
?>