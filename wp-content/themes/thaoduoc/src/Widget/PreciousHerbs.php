<?php 

namespace App\Widget;

use MSC\Widget;

/**
* Show title as a line on homepage
*/
class PreciousHerbs extends Widget
{
	public function __construct()
    {
        $widget = [
            'id'          => 'precious_herbs',
            'label'       => __('Precious Herbs', 'thaoduoc'),
            'description' => 'This widget shows product by category',
        ];

        $fields = [
            [
                'label' => __('ID các category', 'thaoduoc'),
                'name'  => 'IDs',
                'type'  => 'text',
            ],
            [
                'label' => __('Text link more', 'thaoduoc'),
                'name'  => 'text_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Icon link mở rộng', 'thaoduoc'),
                'name'  => 'icon_link_more',
                'type'  => 'text',
            ],
            [
                'label' => __('Số cột chia sản phẩm (max: 12)', 'thaoduoc'),
                'name'  => 'number_column',
                'type'  => 'text',
            ]
        ];

        parent::__construct($widget, $fields);
    }

    /**
     * [handle description]
     * @param  [type] $instance [description]
     * @return [type]           [description]
     */
    public function handle($instance)
    {
        global $post, $wp_query, $product;
        $cat_title =  get_queried_object();
        $cat_slug = $cat_title->slug; 
        if(empty($cat_slug)) {
            $cat_slug = $cat_title->post_name; 
        }
        ?>
        <div class="homepage-product-list container">
            <div class="product-cate-box">
                <div class="row product-cate-list">
                    <?php 
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $array_blog_cat = array(
                        'post_type' => 'product',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'slug',
                                'terms' => $cat_slug,
                            ),
                        ),
                        'posts_per_page'=>12,
                        'paged'=>$paged
                    ); 
                    $get_products = new \WP_Query($array_blog_cat);
                    $max_num_pages = $get_products->max_num_pages;
                    if($get_products->have_posts()) :
                        while($get_products->have_posts()) : $get_products->the_post(); 
                            $product = new \WC_Product($post->ID);
                            $price = $product->get_price();
                            $comment_count = $post->comment_count;
                            if (!empty($get_products)) {
                                $image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                                $link = get_permalink();
                                $col = $instance['number_column'];
                                $no_image = get_template_directory_uri() . '/assets/images/no_image_1.jpg';
                                if(empty($col)) {
                                	$col = 3;
                                }
                                ?>
                                <div class="col-md-<?php echo $col; ?> col-sm-<?php echo $col; ?> col-xs-12 product-column">
                                    <div class="product-item">
                                        <div class="product-cate-img">
                                    		<a href="<?php echo $link; ?>">
    	                                    	<img src="<?php echo asset('images/transparent-product.png'); ?>" 
                                                    style="background: url('<?php echo $image ? $image : $no_image; ?>') no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;" />
    	                                	</a>
    	                                </div>
                                        <div class="product-cate-title">
                                            <a href="<?php echo $link; ?>"><?php echo $post->post_title; ?></a>
                                        </div>
                                        <div class="product-price">
                                            <span class="title-price">Giá bán: </span>
                                            <span class="price-main"><?php echo wc_price($price); ?></span>
                                        </div>
                                       	<div class="row more-info">
                                       		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
                                       			<i class="fa fa-calendar" aria-hidden="true"></i> 
                                       			<span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $post->ID ); ?></span>
                                       		</div>
                                       		<div class="col-md-6 col-sm-6 col-xs-6 pull-right sub-more-right count_buy_wrap">
                                       			<!-- <i class="fa fa-comments" aria-hidden="true"></i>
                                       			<span class="comment-post">
                                       				<?php //echo $comment_count; ?> phản hồi 123
                                       			</span> -->
                                                <!-- <span class="fa fa-user"></span>  -->
                                                <span class="p-view-count"><?php echo show_count_price_product($post->ID); ?></span> người đã mua
                                       		</div>
                                       	</div>
                                    </div>
                                </div>
                                <?php
                            }
                        endwhile;
                    else :
                        echo view('templates.No_product');
                    endif;
                    do_action('custom_paginate', $max_num_pages);
                    ?>
                </div>
            </div>
        </div>
	<?php
    }
}