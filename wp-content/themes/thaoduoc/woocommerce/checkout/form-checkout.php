<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div  id="customer_details" class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 checkout-input-container">
				<?php 
				do_action( 'woocommerce_checkout_billing' ); 
				do_action( 'woocommerce_checkout_shipping' ); 
				?>
				<div id="order_review" class="woocommerce-checkout-review-order">
					<?php do_action( 'woocommerce_checkout_order_review' ); ?>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 info-checkout">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 img-info-checkout">
					<img src="<?php echo asset('images/leaf-checkout-thaoduoc.png'); ?>">
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 content-info-checkout">
					<?php
					if(is_active_sidebar('info-checkout')){
						dynamic_sidebar('info-checkout');
					}
					?>
				</div>
			</div>
		</div>
		<div class="row">

		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
	<?php endif; ?>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
