<?php
/**
 * Show error messages
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/notices/error.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ) {
	return;
}

?>
<ul class="woocommerce-error">
	<?php foreach ( $messages as $message ) : ?>
		<?php
		// var_dump($message);
    	$words = [
    		'is a required field',
    		'Billing',
			'Sorry',
			'this product cannot be purchased',
			'is not in stock',
			'Please edit your cart and try again',
			'We apologize for any inconvenience caused',
		];
    	$replaceWords = [
    		'la truong bat buoc',
    		'',
			'Xin lỗi,
			sản phẩm này tạm thời chưa thể mua được',
			'không có trong kho',
			'Vui lòng chỉnh sửa giỏ hàng của bạn và thử lại',
			'Chúng tôi xin lỗi vì sự bất tiện này',
		];
    	$newMessage = str_replace($words, $replaceWords, $message);
    	?>
		<li><?php echo wp_kses_post( $newMessage ); ?></li>
	<?php endforeach; ?>
</ul>
