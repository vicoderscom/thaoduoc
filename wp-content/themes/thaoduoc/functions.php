<?php
require_once __DIR__ . '/bootstrap/app.php';
require_once __DIR__ . '/bootstrap/array_options.php';
require_once __DIR__ . '/bootstrap/post_type.php';
require_once __DIR__ . '/src/Widget/data_ajax.php';

use App\CustomPostType\HanbookPostType;
use App\CustomPostType\NewsPostType;
use App\CustomPostType\VideoPostType;
use App\Widget\AddressWidget;
use App\Widget\Handbook;
use App\Widget\HerbalShop;
use App\Widget\ListHomepageWidget;
use App\Widget\NewsWidget;
use App\Widget\PhoneWidget;
use App\Widget\PostInvolve;
use App\Widget\PreciousHerbs;
use App\Widget\ProductInvolve;
use App\Widget\ProductSameTopic;
use App\Widget\QuangCaoHome;
use App\Widget\SearchResult;
use App\Widget\TablineHomepageWidget;
use App\Widget\Tag;
use App\Widget\VideoSectionWidget;

/**
 * Setting for theme
 */
class SettingTheme {
	public function __construct() {
		add_theme_support('custom-logo');
		add_filter( 'wp_insert_post_data', [$this, 'default_comments_on'] );

		register_nav_menus( array(
    		'top'    => __('Top Menu', 'thaoduoc'),
    		'menu-footer-2'    => __('Menu footer 2', 'thaoduoc'),
    		'menu-footer-3'    => __('Menu footer 3', 'thaoduoc')
    	) );
    	add_action('widgets_init', [$this, 'set_widget']);	
    	add_action('custom_paginate', [$this, 'custom_paginate'], 10, 1);
        $this->custom_widget(); 
        $this->custom_posttype();
        // add_filter( 'get_comment_date', [$this, 'forDateComment'] ); 
        add_action('action_comment', [$this, 'action_comment'], 10, 1);   
	}
	public function default_comments_on( $data ) {
        $data['comment_status'] = 'open';
	    return $data;
	}
	public function set_widget() {
		$sidebars = [
			[
				'name'          => __('Footer Column 1', 'thaoduoc'),
				'id'            => 'footer-1',
				'description'   => __('This is sidebar for column 1', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Footer Column 2', 'thaoduoc'),
				'id'            => 'footer-2',
				'description'   => __('This is sidebar for column 2', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Footer Column 3', 'thaoduoc'),
				'id'            => 'footer-3',
				'description'   => __('This is sidebar for column 3', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Footer Column 4', 'thaoduoc'),
				'id'            => 'footer-4',
				'description'   => __('This is sidebar for column 4', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Homepage', 'thaoduoc'),
				'id'            => 'homepage',
				'description'   => __('This is all section on homepage', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
            [
                'name'          => __('Precious herbs', 'precious_herbs'),
                'id'            => 'precious_herbs',
                'description'   => __('This is all section on Precious herbs', 'thaoduoc'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Herbal shop', 'herbal_shop'),
                'id'            => 'herbal_shop',
                'description'   => __('This is all section on Herbal shop', 'thaoduoc'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
			[
				'name'          => __('Handbook', 'handbook'),
				'id'            => 'handbook',
				'description'   => __('This is all section on Handbook', 'handbook'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('News', 'news'),
				'id'            => 'news',
				'description'   => __('This is all section on News', 'news'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('SearchResult', 'searchresult'),
				'id'            => 'searchresult',
				'description'   => __('This is all section on Search Result', 'searchresult'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Sidebar Product', 'sidebar_product'),
				'id'            => 'sidebar_product',
				'description'   => __('This is all section on Sidebar Product', 'sidebar_product'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Sidebar post', 'sidebar_post'),
				'id'            => 'sidebar_post',
				'description'   => __('This is all section on Sidebar post', 'sidebar_post'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Tag', 'tag'),
				'id'            => 'tag',
				'description'   => __('This is all section on Sidebar Product', 'tag'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Social', 'thaoduoc'),
				'id'            => 'social',
				'description'   => __('This is all section on social', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Information Checkout Page', 'thaoduoc'),
				'id'            => 'info-checkout',
				'description'   => __('Show information payment on checkout page', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Link - Quy tắc bình luận', 'thaoduoc'),
				'id'            => 'link-quy-tac-comment',
				'description'   => __('Show link rule comment', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],

			[
				'name'          => __('Quảng cáo và liên hệ mua hàng Trang chủ', 'thaoduoc'),
				'id'            => 'quangcao_lienhe',
				'description'   => __('Show link rule comment', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			],
			[
				'name'          => __('Video Giới thiệu - Homepage', 'thaoduoc'),
				'id'            => 'video_section_homepage',
				'description'   => __('Show link rule comment', 'thaoduoc'),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			]

		];

		foreach ($sidebars as $sidebar) {
			register_sidebar($sidebar);
		}
	}

	public function custom_paginate($max_num_pages) {
		echo '<div class="contain_pagination">';
	    	echo '<div class="paginate pull-right">';
	        $total_pages = $max_num_pages;
	        if ($total_pages > 1){

	            $current_page = max(1, get_query_var('paged'));

	            echo paginate_links(array(
	                'base' => get_pagenum_link(1) . '%_%',
	                'format' => 'page/%#%',
	                'current' => $current_page,
	                'total' => $total_pages,
	                'prev_text'    => __('Previous'),
	                'next_text'    => __('Next »'),
	            ));
	        }
		    echo '</div>';
		echo '</div>';
	}

	public function custom_widget() {
		new AddressWidget();
		new PhoneWidget();
		new ListHomepageWidget();
		new TablineHomepageWidget();
		new ProductInvolve();
		new ProductSameTopic();
		new PostInvolve();
        new NewsWidget();
        new Handbook();
        new HerbalShop();
		new PreciousHerbs();
		new SearchResult();
		new Tag();
		new QuangCaoHome();
		new VideoSectionWidget();
	}

	public function custom_posttype() {
		new HanbookPostType();
		new NewsPostType();
		new VideoPostType();
	}

	// public function forDateComment($date) {
	//     $date = date('d-m-Y');  
	//     return $date;
	// }

	public function action_comment($id) {
        // if (is_single ()):
        $url = site_url();
        $comments_args = array(
            'fields' => [
                'author' => '<p class="comment-form-author"><label for="author">' . __( 'Name *', 'domainreference' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
                    '<input id="author" class="form-control" name="author" type="text" style="" value="' . esc_attr( $commenter['comment_author'] ) .
                    '" size="30"' . $aria_req . ' /></p>',
                'email' => '<p class="comment-form-email"><label for="email">' . __( 'Email *', 'domainreference' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) . '<input id="email" class="form-control" name="email" type="text" style="" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
            ],
            'label_submit' => __( 'Gửi bình luận', 'thaoduoc' ),
            'title_reply' => __( 'MỜI BẠN ĐỂ LẠI BÌNH LUẬN', 'thaoduoc' ),
            'comment_notes_after' => '',
            'comment_field' => '<div class="comment-form-comment"><label for="comment">' . _x( 'Bình luận *', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true" class="form-control" style="width: 100%;margin-bottom: 15px;"></textarea></div>',
            'class_form' => 'form',
            'class_submit' => 'btn btn-default',
            'logged_in_as' => '',
            'comment_notes_after' => '<a href="'. (site_url('quy-dinh-dang-binh-luan')) .'" style="float: right;"><i class="fa fa-info-circle" aria-hidden="true"></i> Quy định đăng bình luận</a>',
            'submit_field' => '<span class="form-submit">%1$s %2$s</a>',
            'format' => 'html5',
            'title_reply_to'    => __( 'Trả lời bình luận đến %s' ),
            'cancel_reply_link' => __( 'Hủy bình luận' ),
        );
        comment_form( $comments_args , $id);

        $comments = get_comments(array(
            'post_id' => $id,
            'status' => 'approve',
        ));

        $list_args = [
            'walker'            => new Walker_Comment(),
            'max_depth'         => '3',
            'style'             => 'ul',
            'callback'          => null,
            'end-callback'      => null,
            'type'              => 'all',
            'reply_text'        => 'Trả lời',
            'page'              => '1',
            'per_page'          => '10',
            'avatar_size'       => 48,
            'reverse_top_level' => false,
            'reverse_children'  => '',
            'format'            => 'html5',
            'short_ping'        => false,  
            'echo'              => true,
            'callback' => 'mytheme_comment'
        ];
        
        function mytheme_comment($comment, $args, $depth) {
		    if ( 'div' === $args['style'] ) {
		        $tag       = 'div';
		        $add_below = 'comment';
		    } else {
		        $tag       = 'li';
		        $add_below = 'div-comment';
		    }
		    ?>
		    <<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		    <?php if ( 'div' != $args['style'] ) : ?>
		        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
		    <?php endif; ?>
		    <div class="comment-author vcard">
		        <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
		        <?php printf( __( '<cite class="fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
		    </div>
		    <?php if ( $comment->comment_approved == '0' ) : ?>
		         <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
		          <br />
		    <?php endif; ?>

		    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
		        <?php
		        /* translators: 1: date, 2: time */
		        printf( __('%1$s'), date_format(date_create($comment->comment_date), 'd/m/Y'),  '' ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
		        ?>
		    </div>

		    <?php 
		        comment_text(); 
		        // $id_comment = get_comment_ID();
		        // $get_date = get_comment_date($id_comment);
		    ?>

		    <div class="reply">
		        <?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		    </div>
		    <?php if ( 'div' != $args['style'] ) : ?>
		    </div>
		    <?php endif; ?>
		    <?php
		    }

	        wp_list_comments( $list_args, $comments );
	        // endif;
        ?>
        <label>Bình luận * </label>
        <textarea id="virtual-comment" aria-required="true" class="form-control" style="width: 100%;margin-bottom: 15px;"></textarea>
        <?php
	}
}
new SettingTheme();




$product_home = new ListHomepageWidget;

add_action('wp_ajax_my_action', 'data_ajax');
add_action('wp_ajax_nopriv_my_action', 'data_ajax');



add_filter( 'comment_post', 'comment_notification' ); 

function comment_notification( $comment_ID, $comment_approved ) {

    // Send email only when it's not approved
     if( $comment_approved == 0 ) {
        $admin_email = get_option( 'admin_email' );

		$to = 'hoangquan.qnet@gmail.com';
		$subject = 'The subject';
		$body = 'có người comment';
		$headers = array('Content-Type: text/html; charset=UTF-8');
	 
	}
}


add_action('wp_ajax_myajax', 'myajax');
add_action('wp_ajax_nopriv_myajax', 'myajax');

function myajax() {

  	$product_id = $_POST['id'];

    // Avoid using the global $woocommerce object
    WC()->cart->add_to_cart($product_id);

    return $count;
}

// function SearchFilter($query)   
// {  	
// 	$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
	
//     if ($query->is_search)   
//     {  
//         $query->set('post_type', array('handbook', 'news', 'product'));
//         $query->set('posts_per_page', 12);
//         $query->set('paged', $paged); 
//     }  
//     return $query;  
// }  
// add_filter('pre_get_posts', 'SearchFilter');

// function filter_where_title($where = '') {
//     $query_string = get_search_query();
//     $where .= " OR post_title LIKE '%".$query_string."%'";                   
//     return $where;
// }