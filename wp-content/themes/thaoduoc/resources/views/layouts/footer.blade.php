<div class="footer">
        <div class="footer-widgets">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="logo col-md-3 col-sm-3 col-xs-12">
                                <a class="site-url" href="{{ get_site_url() }}">
                                    <?php
                                    $customLogoId = get_theme_mod('custom_logo');
                                    $logo = wp_get_attachment_image_src($customLogoId , 'full');

                                    if (has_custom_logo()) {
                                        echo '<img src="'. esc_url($logo[0]) .'" width="100%" height="100%">';
                                    } else {
                                        echo '<h1>'. esc_attr(get_bloginfo('name')) .'</h1>';
                                    }
                                    ?>
                                </a>
                            </div>
                            <div class="logo col-md-12 col-sm-12 col-xs-12">
                                <?php dynamic_sidebar('footer-1'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php dynamic_sidebar('footer-2'); ?>
                        <p class="title-footer-search">Tìm kiếm sản phẩm</p>
                        <div class="search-form">
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <?php dynamic_sidebar('footer-3'); ?>
                         <?php dynamic_sidebar('footer-4'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
