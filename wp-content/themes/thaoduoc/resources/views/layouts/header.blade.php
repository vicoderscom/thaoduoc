<?php 
    $count_cart = WC()->cart->get_cart_contents_count();
    $current_product = get_woocommerce_currency_symbol();
?>
<div class="col-md-12 col-sm-12 col-xs-12 line-top"></div>
<div class="header">
    <div class="container">
        <div class="logo col-md-3 col-sm-3 col-xs-12">
            <a class="site-url" href="{{ get_site_url() }}">
                <?php
                $customLogoId = get_theme_mod('custom_logo');
                $logo = wp_get_attachment_image_src($customLogoId , 'full');

                if (has_custom_logo()) {
                    echo '<img src="'. esc_url($logo[0]) .'" width="100%" height="100%">';
                } else {
                    echo '<h1>'. esc_attr(get_bloginfo('name')) .'</h1>';
                }
                ?>
            </a>
        </div>
        <div class="search-form col-md-4 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-12">
            <?php get_search_form(); ?>
        </div>
        <div class="cart hidden-xs col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">
            <a href="<?php echo get_page_link(5); ?>">
                <div class="cart-icon">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </div>
                <div class="cart-total">
                    <span class="cart-text"><?php echo ($count_cart > 0) ? '('.$count_cart.')' : '';  ?><span><?php _e('Giỏ hàng', 'thaoduoc'); ?></span></span>
                </div>
            </a>
        </div>
    </div>
    <div class="menu-header">
        <div class="container">

                <div class="main-menu">
                    <?php
                    wp_nav_menu([
                        'theme_location' => 'top',
                        'menu_id'        => 'top-menu',
                        'menu_class'     => 'nav-menu',
                    ]);
                    ?>
                </div>
                <div class="mobile-menu"></div>
                
    <!--    <nav class="navbar navbar-default top-menu">
            <div class="row">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php
                    wp_nav_menu([
                        'theme_location' => 'top',
                        'menu_id'        => 'top-menu',
                        'menu_class'     => 'nav-menu',
                    ]);
                    ?>
                </div>
            </div>
        </nav> -->

        </div>
    </div>
</div>