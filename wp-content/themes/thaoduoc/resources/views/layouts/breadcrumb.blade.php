<?php  
if(!is_home() || !is_front_page()):
?>
<div class="breadcrumb">
	<div class="container">
		<div class="row breadcrumb-row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breadcrumb-col">
				<?php
				$args = [
				    'delimiter' => ' > '
				];
				woocommerce_breadcrumb( $args );
				?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>