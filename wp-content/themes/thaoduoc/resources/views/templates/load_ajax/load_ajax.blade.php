@if(is_home())
@php
	$url_site = get_site_url();
@endphp
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
	var offset = 0;
	var pageNumber = 1;
	var ppp = 12;
	var url_site = "<?php echo $url_site; ?>";
	console.log(url_site);
	function fetch_data() {
		offset += 12;
		var data_url = url_site + "/wp-admin/admin-ajax.php";
		var str = 'ppp=' + ppp + '&pageNumber=' + pageNumber + '&action=my_action'+'&offset='+offset;
		try {
			$.ajax({
				type: "POST",
				url: data_url,
				data: str,
				success: function(response) {
					if(response) {
					    $('#product_category_widget-2').append(response);
						$('.img_load_state_icon').hide();
					}
				},
				error: function(error) {
		            console.log(error);            
		        }
			});
		} catch(e) {
			alert('You messed something up');
		}
	}
	$('#load_more').click(function(){
		fetch_data();
		$('.img_load_state_icon').show();
	});

</script>
@endif