<?php
/*
Template Name: Search Page
*/

get_header(); 
?>

<div class="page_search bg_wraper">
    <?php
    	if (is_active_sidebar('searchresult')) {
    		dynamic_sidebar('searchresult');
    	}
    ?>
</div>

<?php
	get_footer();
?>