<div class="page_contact bg_wraper">
    <div class="container">
    	<div class="col-md-12">
    		<div class="row">
    			<div class="content_contact">
    				<div class="col-md-6">
    					<div class="row">
                            <div class="contact_info">
                                <div class="bg_contact_info">
                                    <p>Chủ sở hữu: Nguyễn Hoàng Quân.</p>
                                    <p>Địa chỉ: Phố mỏ Nhài, Hưng Vũ, Bắc Sơn, Lạng Sơn</p>
                                    <p>Điện thoại: 0971.050.999</p>   
                                    <p>Giấy CN ĐKKD số: 14E8002826</p>   
                                </div>
                            </div>
                            <div class="map">
        						@php
        							echo do_shortcode('[wpgmza id="1"]');
        						@endphp
                            </div>
    					</div>
    				</div>
    				<div class="col-md-6">
    					<div class="row">
                            <div class="contact_desc">
                                <h3>Thông tin khách hàng</h3>
        						@php
        							echo do_shortcode('[contact-form-7 id="109" title="Contact form 1"]');
        						@endphp
                            </div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
