<div class="homepage bg_wraper">
    <div class="feature_news">
    	<div class="container">
    		<div class="col-md-12 wrap_home">
    			<div class="row">
    				<div class="col-md-8 col-sm-8 col_feature_product">
						<div class="feature">
							@php
							    $feature = [
									'post_type'      => 'product',
									'post_status'    => 'publish',
									'posts_per_page'        => 1,
									'meta_query' => [
										[
											'key'       => 'feature_home',
											'value'     => ['', null],
											'compare'        => '!='
										]
									]
								];
								$get_product = new WP_Query($feature);
								$product_feature = [];
								$no_image = get_template_directory_uri() . '/assets/images/no_image_1.jpg';
								if($get_product->have_posts()):
									while($get_product->have_posts()): $get_product->the_post();
	    								$check_field = get_field('feature_home');
	    								if(!empty($check_field)) {
	    									array_push($product_feature, $get_product->post);
	    								}
								    endwhile;
								endif;
								foreach($product_feature as $val_feature) {
									$val_feature->image = wp_get_attachment_url(get_post_thumbnail_id($val_feature->ID));
									$val_feature->link = get_permalink($val_feature->ID);
									$product = new \WC_Product($val_feature->ID);
                                    $val_feature->price = $product->get_price();
                                    $val_feature->comment = $val_feature->comment_count;
								}
							@endphp
                            <div class="product-item">
                                <div class="product-cate-img">
                            		<a href="<?php echo $product_feature[0]->link; ?>">
                                    	<img src="<?php echo asset('./images/trans_feature.png'); ?>" 
                                    	     style="background: url('<?php echo $product_feature[0]->image ? $product_feature[0]->image : $no_image; ?>')" />
                                	</a>
                                </div>
                                <div class="content">
                                    <div class="col-md-12 col-md-12 col-xs-12 content_price_title">
	                                    <div class="product-cate-title">
	                                        <a href="<?php echo $product_feature[0]->link; ?>"><?php echo $product_feature[0]->post_title; ?></a>
	                                    </div>
	                                    <div class="product-price">
	                                        <span class="title-price">Giá bán: </span>
	                                        <span class="price-main"><?php echo wc_price($product_feature[0]->price); ?></span>
	                                    </div>
	                                </div>
                                   	<div class="row more-info">
                                   	    <div class="col-md-12 col-sm-12 col-xs-12">
	                                   		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
	                                   			<i class="fa fa-calendar" aria-hidden="true"></i> 
	                                   			<span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $product_feature[0]->ID ); ?></span>
	                                   		</div>
	                                   		<div class="col-md-3 col-sm-3 col-xs-6 pull-right sub-more-right text-right count_buy_wrap">
					                   			{{-- <span class="fa fa-user"></span>  --}}
					                   			<span class="p-view-count">{!! show_count_price_product($product_feature[0]->ID) !!}</span> người đã mua
					                   		</div>
	                                   	</div>
                                   	</div>
                                </div>
                            </div>
						</div>
    				</div>
    				<div class="col-md-4 col-sm-4 col_home_news">
					    <div class="waper_product_involve bg_detai">
							<div class="list_item">
    						@php
    							$feature = [
									'post_type'      => 'news',
									'post_status'    => 'publish',
									'meta_key'       => 'tin_tuc_trang_chu'
								];
								$get_product = new WP_Query($feature);
								$news_home = [];
								$no_image = get_template_directory_uri() . '/assets/images/no_image_1.jpg';
								if($get_product->have_posts()):
									while($get_product->have_posts()): $get_product->the_post();
								        $check_field = get_field('tin_tuc_trang_chu');
	    								if(!empty($check_field)) {
	    									array_push($news_home, $get_product->post);
	    								}
							        endwhile;
								endif;
								$map_news_home = array_slice($news_home, 0, 4);
								foreach($map_news_home as $news_home_val) {
									$news_home_val->image = wp_get_attachment_url(get_post_thumbnail_id($news_home_val->ID));
									$news_home_val->link = get_permalink($news_home_val->ID);
									$news_home_val->comment = $news_home_val->comment_count;
								}
    						@endphp
    						@foreach($map_news_home as $news_home_val)
    						    <div class="item_list">
					                <div class="col-md-12">
					                    <div class="row">
							                <div class="col-md-5 col-sm-12 col-xs-12">
							                    <div class="row">
							                        <a href="{{ $news_home_val->link }}">
										            	<div class="img bg_img" style="background-image: url(<?php echo $news_home_val->image ? $news_home_val->image : $no_image; ?>)">
										            		<img class="item_img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/news_home.png" alt="">
										            	</div>
										            </a>
									            </div>
								            </div>
							            	<div class="col-md-7 col-sm-12 col-xs-12">
								            	<div class="info">
								            		<h1 class="title">
								            		    <a href="{{ $news_home_val->link }}">
								            		        <?php echo $news_home_val->post_title; ?>
								            		    </a>
								            		</h1>
								            	</div>
								            	<div class="row more-info">
			                                   		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left calander">
			                                   			<i class="fa fa-calendar" aria-hidden="true"></i> 
			                                   			<span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $news_home_val->ID ); ?></span>
			                                   		</div>
			                                   	</div>
								            </div>
					                    </div>
					                </div>
					            </div>
    						@endforeach
    					</div>
    				</div>
    			</div>


    			<div class="news_mobile">
    				<div class="container">
    					<div class="row news_home_mobile">
    						@foreach($map_news_home as $news_home_val)

	    						<div class="col-md-4 col-sm-4 col-xs-12 product-column">
								    <div class="product-item">
								            <div class="product-cate-img">
								        		<a href="{{ $news_home_val->link }}">
								              		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/transparent-product.png" style="background: url(<?php echo $news_home_val->image ? $news_home_val->image : $no_image; ?>) no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;">
								          		</a>
								          	</div>
								            <div class="product-cate-title">
								                <a href="{{ $news_home_val->link }}"><?php echo $news_home_val->post_title; ?></a>
								            </div>
								           	<div class="row more-info">
								           		<div class="col-md-6 col-sm-6 col-xs-6 pull-left sub-more-left">
								           			<i class="fa fa-calendar" aria-hidden="true"></i> 
								           			<span class="datetime-post"><?php echo get_the_date( 'd/m/Y', $news_home_val->ID ); ?></span>
								           		</div>
								           	</div>
								    </div>
								</div>

							@endforeach

    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    </div>
	@if(is_active_sidebar('homepage'))
		<?php dynamic_sidebar('homepage'); ?>
	@endif
	<div class="more_post">
        <div class="col-md-12">
            <div class="row">
                <div class="button_load_more">
                    <button id="load_more"><img class="img_load_state_icon" src="{{ get_stylesheet_directory_uri() }}/assets/images/loading-icon-6437022dfef658c9c8f2098bd8033472e853eb8bb959fc4908e2bdb94adae67d.gif" alt=""> Xem Thêm</button>
                </div>
            </div>
        </div>
    </div>
    @include('templates.load_ajax.load_ajax')
	@php
		global $post, $wp_query, $product;
	@endphp

    <div class="hot_product">
		<div class="container list_item_noi_bat">
			<p class="title_hot">Sản phẩm nổi bật</p>
			<div class="row slick_product">
				@php
	                $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
	                $offset = (isset($_POST['offset'])) ? $_POST['offset'] : 0;
	                $args_pro = [
	                    'post_type'             => 'product',
	                    'post_status'           => 'publish',
	                    'posts_per_page'        => 15,
	                    'orderby' => 'desc',
	                    'meta_query' => [
                            [
                                'key' => 'hot_product_home',
                                'value' => ['', null],
                                'compare' => '!='
                            ]
                        ]
	                ];
	                $get_products = new \WP_Query($args_pro);
	                $max_num_pages = $get_products->max_num_pages;
	                if (!empty($get_products)) {
	                    foreach ($get_products->posts as $key => $pro) {
	                        $id = $pro->ID;
	                        $title = $pro->post_title;
	                        $img = wp_get_attachment_url(get_post_thumbnail_id($id));
	                        $img = ($img) ? $img : 'http://placehold.it/370x235';
	                        $url = get_permalink($pro->ID);
	                        $comment_count = $pro->comment_count;
	                        $hot_product = get_field( 'hot_product_home', $pro->ID );

	                        $data = [
	                                'id' => $id,
	                                'title' => $title,
	                                'img'   => $img,
	                                'url'   => $url
	                            ];

	                        $col = $instance['number_column'];
	                        if(empty($col)) {
	                        	$col = 3;
	                        }
	                        $post_type = $pro->post_type;

	                        if($post_type == 'product') {
	                            $product = new \WC_Product($pro->ID);
	                            $price = $product->get_price();
	                        }
					
					if($hot_product[0] === 'co'){
				@endphp

					<div class="col-md-3 col-sm-4 col-xs-12 product-column">
		                <div class="product-item">
		                    <div class="product-cate-img">
		                		<a href="<?php echo $url; ?>">
		                        	<img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url('<?php echo $img; ?>') no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 235px;" />
		                    	</a>
		                    </div>
		                    <div class="product-cate-title" style="margin-bottom: 0;">
		                        <a href="<?php echo $url; ?>"><?php echo $title; ?></a>
		                    </div>
		                    <div class="product-price">
		                        <span class="title-price">Giá bán: </span>
		                        <span class="price-main"><?php echo wc_price($price); ?></span>
		                    </div>
		                   	<div class="row more-info">
		                   		<div class="col-md-6 col-sm-6 col-xs-12 pull-left sub-more-left">
		                   			<i class="fa fa-calendar" aria-hidden="true"></i> 
		                   			<span class="datetime-post">
		                                <?php echo get_the_date( 'd/m/Y', $pro->ID ); ?>
		                            </span>
		                   		</div>
		                   		<div class="col-md-6 col-sm-6 col-xs-12 text-right count_buy_wrap">
		                   			{{-- <span class="fa fa-user"></span>  --}}
		                   			<span class="p-view-count">{!! show_count_price_product($id) !!}</span> người đã mua
		                   		</div>
		                   	</div>
		                </div>
		            </div>

				@php

						}
						}
	                }
				@endphp
			</div>
		</div>
	</div>

	@if(is_active_sidebar('quangcao_lienhe'))
        <?php dynamic_sidebar('quangcao_lienhe'); ?>
    @endif

    @if(is_active_sidebar('video_section_homepage'))
        <?php dynamic_sidebar('video_section_homepage'); ?>
    @endif
    
    {{-- <div class="container">
    	<div class="tag_hot">
		    <ul class="list_tag">
    			<li class="title_name">Tìm nhiều nhất: </li>
	    		@php
	    			$params = [
					    'hierarchical'     => 1,
					    'show_option_none' => '',
					    'hide_empty'       => 0,
					    'parent'           => 0,
					    'taxonomy'         => 'product_tag',
					    'meta_key' 		   => 'hot_tag'
					];

					$categories = get_categories($params);

					foreach ($categories as $key => $value) {

					$check_tag = get_field('hot_tag', 'product_tag_' . $value->term_id);
					$url = get_category_link($value->term_id);

						if ($check_tag[0] === 'co') {
	    		@endphp

			    		<li><a href="{{ $url }}">{{ $value->name }}</a></li>

	    		@php
	    				}
	    			}
	    		@endphp
		    </ul>
    	</div>
    </div> --}}
</div>

