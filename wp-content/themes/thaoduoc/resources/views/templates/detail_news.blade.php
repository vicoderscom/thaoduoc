@php
    $img_product = wp_get_attachment_url(get_post_thumbnail_id());
    $url = home_url();
    $no_image = get_template_directory_uri() . '/assets/images/no_image_1.jpg';
    $getlink_cmt = get_field('link_comment_facebook');
    if(!empty($getlink_cmt)) {
        $link_prod = $getlink_cmt;
    } else {
        $link_prod = get_permalink();
    }
@endphp
        <div class="product_detail bg_wraper">
            <div class="container pad-l-0 pad-r-0">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12 pad-l-0 pad-r-0 wraper_content col_product_detail">
                            <div class="detail_product bg_detai">
                                <div class="col-md-12">
                                    <div class="row">
                                        @php
                                            if(have_posts()) : while(have_posts()) : the_post();
                                        @endphp
                                        <div class="col-md-12 col_product_detail_1">
                                            <div class="row">
                                                <div class="item_list_description">
                                                    <div class="col-md-5 col-sm-12">
                                                        <div class="img bg_img" style="background-image: url({{ $img_product ? $img_product : $no_image }})">
                                                            <img class="item_img" src="{{ get_stylesheet_directory_uri() }}/assets/images/detail/trans.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-sm-12">
                                                        <div class="info">
                                                            <h1 class="title">@php the_title(); @endphp</h1>
                                                            <div class="description">@php the_excerpt(); @endphp</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col_product_detail_2">
                                            <div class="row">
                                                <div class="description_product">
                                                    <div class="col-md-12">
                                                        @php 
                                                            the_content(); 
                                                        @endphp
                                                    </div>
                                                    <div class="content_more">
                                                        <span id="show_content_post">Đọc thêm <i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            endwhile;
                                                endif; 
                                        @endphp
                                        {{-- <div class="col-md-12 col_product_detail_3">
                                            <div class="row">
                                                <div class="banner">
                                                    {!! do_shortcode('[bc_random_banner]') !!}
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class="container-fluid comment_contact_news">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="share_add_cart">
                                                        <h3>
                                                            <marquee>
                                                                {!! get_option('bottomad') !!}
                                                            </marquee>
                                                        </h3>
                                                        <div class="detail">
                                                            <div class="social_button">
                                                                @php
                                                                   if(is_active_sidebar('social')) :
                                                                       dynamic_sidebar('social');
                                                                   endif 
                                                                @endphp

<div class="social-share-facebook">
    <div class="btfbl">
        <div class="fb-share-button" data-href="<?php the_permalink();?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Chia sẻ</a></div>
    </div>
</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="tag_involve">
                                                        @php
                                                            $post_current = get_queried_object();
                                                            $post_current_id = get_queried_object()->ID;  
                                                            if($post_current->post_type == 'handbook') {
                                                                $cates = get_the_terms($post_current_id, 'category_handbook');
                                                            } else {
                                                                $cates = get_the_terms($post_current_id, 'category_news');
                                                            }
                                                            if(!empty($cates)) {
                                                                foreach($cates as $cate) {
                                                                    $cate->link = get_category_link($cate->term_id);
                                                                }
                                                            }
                                                        @endphp
                                                        @if(!empty($cates)) 
                                                        <div>DANH MỤC: 
                                                                @foreach($cates as $cate)
                                                                    <span><a href="{{ $cate->link }}">{!! $cate->name !!}</a></span> 
                                                                @endforeach
                                                        </div>
                                                        @endif
                                                        TỪ KHÓA:
                                                        @php
                                                            $post_news = $post_current_id;
                                                            $cates_Tag = get_the_terms($post_news, 'product_tag');
                                                        @endphp
                                                        @if(!empty($cates_Tag))
                                                            @foreach($cates_Tag as $val_tag) 
                                                                @php
                                                                    $link = get_term_link($val_tag->term_id);
                                                                @endphp
                                                                <a href="{{ $link }}">{!! $val_tag->name !!}</a>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 product_lienquan">
                                                <p class="title_danhmuc">Sản phẩm liên quan</p>
                                                <div class="row slick_product_detail">
                                                @php
                                                    $cates_news = get_the_terms($post_current_id, 'product_tag');

                                                    $check_exit = [];

                                                    foreach ($cates_news as $key => $value_news) {
                                                        $args = [
                                                            'post_type'      => array('product'),
                                                            'posts_per_page' => 10,
                                                            'tax_query'      => array(
                                                                array(
                                                                    'taxonomy'         => 'product_tag',
                                                                    'terms'            => array($value_news->slug),
                                                                    'field'            => 'slug',
                                                                ),
                                                            ),
                                                        ];

                                                        $loop_news = new WP_Query($args);

                                                        foreach ($loop_news->posts as $key => $post_news) {

                                                            if(!in_array($post_news->ID, $check_exit)){
                                                                $check_exit[] = $post_news->ID;

                                                            $tite_news = get_the_title($post_news->ID);
                                                            $url = get_permalink($post_news->ID);

                                                            $img = wp_get_attachment_url(get_post_thumbnail_id($post_news->ID));
                                                            $img = ($img) ? $img : 'http://placehold.it/370x235';
                                                            $title = get_the_title($post_news->ID);

                                                            $product = new \WC_Product($post_news->ID);
                                                            $price = $product->get_price();

                                                @endphp
                                                            <div class="col-md-3 col-sm-3 col-xs-6 product-column">
                                                                <div class="product-item">
                                                                    <div class="product-cate-img">
                                                                        <a href="{{ $url }}">
                                                                            <img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url({{ $img }}) no-repeat center center; background-size: cover;width: 100%;max-height: 135px;overflow: hidden;height: 235px;">
                                                                        </a>
                                                                    </div>
                                                                    <div class="product-cate-title" style="">
                                                                        <a href="{{ $url }}">{{ $title }}</a>
                                                                    </div>
                                                                    <div class="product-price">
                                                                                                            <span class="title-price">Giá bán: </span>
                                                                        <span class="price-main"><span class="woocommerce-Price-amount amount">{!! wc_price($price) !!}<span class="woocommerce-Price-currencySymbol"></span></span></span>
                                                                                                        </div>
                                                                    <div class="row more-info">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 sub-more-left">
                                                                            <i class="fa fa-calendar" aria-hidden="true"></i> 
                                                                            <span class="datetime-post">{{ get_the_date( 'd/m/Y', $post_news->ID ) }}</span>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 sub-more-right">
                                                                            <i class="fa fa-comments" aria-hidden="true"></i>
                                                                            <!-- <span class="comment-post">
                                                                                 phản hồi
                                                                            </span> -->
                                                                            <span class="fb-comments-count" data-href="{{ $url }}" fb-xfbml-state="rendered"><span class="fb_comments_count"></span></span> phản hồi
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                @php
                                                                }
                                                            }
                                                    }
                                                @endphp
                                                </div>
                                            </div>

                                            <div class="col-md-12 tintuc_lienquan">
                                                <p class="title_danhmuc">Bài viết cùng chủ đề</p>

                                                <div class="row slick_news_detail">
                                                    @php
                                                        foreach ($cates as $key => $value) {

                                                            if($post_current->post_type == 'handbook'){
                                                                $args = [
                                                                    'post_type'      => array('handbook'),
                                                                    'posts_per_page' => 10,
                                                                    'tax_query'      => array(
                                                                        array(
                                                                            'taxonomy'         => 'category_handbook',
                                                                            'terms'            => array($value->term_id),
                                                                            'field'            => 'term_id',
                                                                        ),
                                                                    ),
                                                                ];
                                                            }else{
                                                                $args = [
                                                                    'post_type'      => array('news'),
                                                                    'posts_per_page' => 10,
                                                                    'tax_query'      => array(
                                                                        array(
                                                                            'taxonomy'         => 'category_news',
                                                                            'terms'            => array($value->term_id),
                                                                            'field'            => 'term_id',
                                                                        ),
                                                                    ),
                                                                ];
                                                            }

                                                            $loop = new WP_Query($args);

                                                            while ($loop->have_posts()): $loop->the_post();

                                                                if ($post_current_id == get_the_ID()) {
                                                                    continue;
                                                                }

                                                                $id = get_the_ID();

                                                                $title = get_the_title();

                                                                $img = wp_get_attachment_url(get_post_thumbnail_id($id));
                                                                $img = ($img) ? $img : 'http://placehold.it/370x235';

                                                                $url = get_permalink($id);
                                                    @endphp

                                                        <div class="col-md-3 col-sm-3 col-xs-6 product-column">
                                                            <div class="product-item">
                                                                    <div class="product-cate-img">
                                                                        <a href="{{ $url }}">
                                                                        <img src="<?php echo asset('images/transparent-product.png'); ?>" style="background: url({{ $img }}) no-repeat center center; background-size: cover;width: 100%;max-height: 235px;overflow: hidden;height: 135px;">
                                                                    </a>
                                                                  </div>
                                                                    <div class="product-cate-title">
                                                                        <a href="{{ $url }}">{{ $title }}</a>
                                                                    </div>
                                                                    <div class="row more-info">
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 sub-more-left">
                                                                            <i class="fa fa-calendar" aria-hidden="true"></i> 
                                                                            <span class="datetime-post">{{ get_the_date( 'd/m/Y', $id ) }}</span>
                                                                        </div>
                                                                        <div class="col-md-6 col-sm-6 col-xs-6 sub-more-right">
                                                                            <span class="fb-comments-count" data-href="{{ $url }}" fb-xfbml-state="rendered"><span class="fb_comments_count"></span></span> phản hồi
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>

                                                    @php
                                                            endwhile;
                                                        }
                                                    @endphp
                                                </div>
                                            </div>



                                            <div class="col-md-12">
                                                <br>
                                                <div class="row">
                                                    <div class="question-answer">
                                                        <div class="col-xs-12 col-sm-6 pull-left pad-l-0">
                                                            <h4 class="text_moi">MỜI BẠN ĐỂ LẠI BÌNH LUẬN</h4>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 pull-right">
                                                            <a href="{{ site_url('quy-dinh-dang-binh-luan') }}" style="float: right;"><i class="fa fa-info-circle" aria-hidden="true"></i> Quy định đăng bình luận</a>
                                                        </div>
                                                        <?php 
                                                        // do_action('action_comment', get_the_ID());
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fb-comments" data-href="{{ $link_prod }}" data-width="100%" data-numposts="5"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            	</div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-4 col-xs-12 col_post_sidebar">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="sidebar">
                                        <div class="waper_product_same_topic waper_product_involve bg_detai">
                                            <div class="product_involve">
                                                <h1>sản phẩm nổi bật</h1>
                                            </div>
                                            <div class="list_item">
                                                <div class="item_list">
                                                    @php
                                                        $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
                                                        $offset = (isset($_POST['offset'])) ? $_POST['offset'] : 0;
                                                        $args_pro = [
                                                            'post_type'             => 'product',
                                                            'post_status'           => 'publish',
                                                            'posts_per_page'        => 10,
                                                            'orderby' => 'desc',
                                                            'meta_query' => [
                                                                [
                                                                    'key' => 'hot_product_home',
                                                                    'value' => ['', null],
                                                                    'compare' => '!='
                                                                ]
                                                            ]
                                                        ];
                                                        $get_products = new \WP_Query($args_pro);
                                                        $max_num_pages = $get_products->max_num_pages;
                                                        if (!empty($get_products)) {
                                                            foreach ($get_products->posts as $key => $pro) {
                                                                $id = $pro->ID;
                                                                $title = $pro->post_title;
                                                                $img = wp_get_attachment_url(get_post_thumbnail_id($id));
                                                                $img = ($img) ? $img : 'http://placehold.it/370x235';
                                                                $url = get_permalink($pro->ID);
                                                                $comment_count = $pro->comment_count;
                                                                $hot_product = get_field( 'hot_product_home', $pro->ID );

                                                                $data = [
                                                                        'id' => $id,
                                                                        'title' => $title,
                                                                        'img'   => $img,
                                                                        'url'   => $url
                                                                    ];

                                                                $col = $instance['number_column'];
                                                                if(empty($col)) {
                                                                    $col = 3;
                                                                }
                                                                $post_type = $pro->post_type;

                                                                if($post_type == 'product') {
                                                                    $product = new \WC_Product($pro->ID);
                                                                    $price = $product->get_price();
                                                                }
                                                        
                                                        if($hot_product[0] === 'co'){
                                                    @endphp
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-5 col-sm-12">
                                                                <div class="row">
                                                                    <a href="<?php echo $url; ?>">
                                                                        <div class="img bg_img" style="background-image: url(<?php echo $img ? $img : $no_image; ?>)">
                                                                            <img class="item_img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/detail/trans.png" alt="">
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7 col-sm-12">
                                                                <div class="info">
                                                                    <h1 class="title">
                                                                        <a href="<?php echo $url; ?>">
                                                                           <?php echo $title; ?>
                                                                        </a>
                                                                    </h1>
                                                                    <div class="product-price">
                                                                        <span class="title-price">Giá bán: </span>
                                                                        <span class="price-main"><?php echo wc_price($price); ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @php
                                                            }
                                                            }
                                                        }
                                                    @endphp
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

