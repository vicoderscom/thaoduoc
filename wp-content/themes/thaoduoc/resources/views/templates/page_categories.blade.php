<div class="page_categories">
	<div class="bg_cate bg_wraper">
		<div class="container pad-l-0 pad-r-0">
			<div class="col-md-12">
				<div class="row">
				    <div class="">
						<div class="col-md-8 col-sm-8 col-xs-12 page_list_cat">
						    <div class="list_cate">
							    <h3>{!! get_the_title() !!}</h3>
								@php
									wp_nav_menu([
								        'menu' => 'Categories All',
								        'menu_class'     => 'nav-menu',
								        'menu_id'        => 'top-menu'
								    ]);
								@endphp
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 page_sidebar_cat">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="sidebar">
                                        @if(is_active_sidebar('sidebar_product'))
                                            <?php dynamic_sidebar('sidebar_product'); ?>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
				  