jQuery(document).ready(function($){
	$('.checkout-input-container label').css({
		'color': '#42902D',
		'font-family': 'segoe',
		'font-size': '16px'
	});
	$('.checkout-input-container input, .checkout-input-container textarea').addClass('form-control').css({'border-color': '#ddd'});
	$('.checkout-input-container textarea').css({'height': '7em'});
});



