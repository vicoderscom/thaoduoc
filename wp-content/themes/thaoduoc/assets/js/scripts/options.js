jQuery(function($) {
    var height_footer = $('#footer_site').height() - 20;
    var icon_scroll = $('#wpfront-scroll-top-container');
    icon_scroll.wrap("<div id='scroll_top_js'></div>");
    icon_scroll.css({
        bottom: height_footer,
        dipslay: 'block'
    });

    // button scroll top
    function scrolltop() {

        var offset = 220;
        var duration = 500;

        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('#button_scroll_top').fadeIn(duration);
            } else {
                jQuery('#button_scroll_top').fadeOut(duration);
            }
        });

        jQuery('#button_scroll_top').click(function(event) {
            event.preventDefault();
            jQuery('html, body').animate({ scrollTop: 0 }, duration);
            return false;
        });
    }
    scrolltop();

    $('.icon_phone').hover(function() {
        $(this).addClass('active_phone');
    }, function() {
        $(this).removeClass('active_phone');
    });

    // $('.navigation-section .menu-menu-main-container ul li').hover(function() {
    //     if ($(this).has('ul')) {
    //         $(this).children('ul').fadeIn();
    //     }
    // }, function() {
    //     $(this).children('ul').hide();
    // });

    $('.woocommerce-cart-form table thead tr th').append('<i class="fa fa-chevron-right" aria-hidden="true"></i>');

    var number_plus = $('.quantity .fa-plus-square');
    var number_minus = $('.quantity .fa-minus-square');
    var val_input_quantity = $('.quantity input').val();

    // custom page cart
    // var plus_square $('.fa-plus-square')
    // end custom page cart

    $('.plus').on('click', function(e) {

        var val = parseInt($(this).prev('input').val());

        $(this).prev('input').val(val + 1);

        $('.update-cart').removeAttr('disabled');
        $('input[name="update_cart"]').removeAttr('disabled');

    });

    $('.minus').on('click', function(e) {

        var val = parseInt($(this).next('input').val());

        if (val !== 0) {
            $(this).next('input').val(val - 1);
        }

        $('.update-cart').removeAttr('disabled');
        $('input[name="update_cart"]').removeAttr('disabled');

    });

    $('.product-column').mousemove(function(e) {

        var parentOffset = $(this).offset();
        var relX = e.pageX - parentOffset.left + 10;
        var relY = e.pageY - parentOffset.top;

        $(this).children('.product-item-hidden').css({
            'top': relY + 'px',
            'left': relX + 'px'
        }).show();
    }).mouseout(function() {
        $(this).children('.product-item-hidden').hide();
    });

    $('.btn-buy').click(function(e) {
        e.preventDefault();

        var productId = $(this).attr('data-id');

        var data = {
            action: 'ajax_add_to_cart_single',
            productId: productId,
        };

        $.post(ajax_obj.ajaxurl, data, function(resp) {
            var response = $.parseJSON(resp);

            if (response.status === 0) {
                window.location = response.redirect;
            }
        });
    });

    function search_auto() {
        var width_form_search = $('#searchform').width();
        $('#searchform').css({
            'left': '50%',
            'margin-left': -(width_form_search / 2)
        });
    }
    search_auto();
    $(window).resize(function(){
        search_auto();
    });

    function set_feature() {
        var home_window_width = window.innerWidth;
        var height_news_home = $('.waper_product_involve').height();
        var height_content_feature = $('.feature .content').height() + 30;
        var height_image_feature = height_news_home - height_content_feature;
        var elment_img = $('.feature .product-cate-img img');
        var news_img_home = $('.feature_news .waper_product_involve .item_img');
        if(home_window_width > 992) {
            elment_img.css({
                height: height_image_feature,
            });
            news_img_home.css({height: 'auto'});
        } else if(home_window_width < 992 && home_window_width > 767) {
            news_img_home.css({height:'60px'});
            var news_change = $('.waper_product_involve').height();
            var feature_img_change = news_change - height_content_feature;
            elment_img.css({height: feature_img_change});
        } else {
            news_img_home.css({height:'auto'});
            elment_img.css({height:'auto'});
        }
    }
    set_feature();
    $(window).resize(function(){
        set_feature();
    });
    setTimeout(function(){
        set_feature();
    },500);
    
    var description_product = $('.description_product').height();
    if(description_product < 1000) {
        $(this).find('.content_more').hide();
    } 
    $('#show_content_post').click(function(){
        $(this).parents('.description_product').css({
            maxHeight: '100%'
        });
        $(this).parent().hide();
    });

    $('.woocommerce-cart-form').parents('.page').addClass('page_cart_list');
    $('form#commentform').children('a').hide();
    
    var breadcrumb_handbook_old = $('.woocommerce-breadcrumb').html();
    if(breadcrumb_handbook_old) {
        if(breadcrumb_handbook_old.replace('Handbook', 'Cẩm nang sức khoẻ')) {
            breadcrumb_handbook = breadcrumb_handbook_old.replace('Handbook', 'Cẩm nang sức khoẻ');
            $('.woocommerce-breadcrumb').html(breadcrumb_handbook);
        }
    }
    var breadcrumb_news_old = $('.woocommerce-breadcrumb').html();
    if(breadcrumb_news_old) {
        if(breadcrumb_news_old.replace('News', 'Tin tức')) {
            breadcrumb = breadcrumb_news_old.replace('News', 'Tin tức'); 
            $('.woocommerce-breadcrumb').html(breadcrumb);
        }
    }
    
    $('.question-answer form a').attr('style', 'display:block !important;float:right');

    $('.hot_product .container .slick_product').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        pauseOnHover: true,
        autoplay: true,
        responsive: [
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
            }
        }
        ]        
    });

    $('.product_lienquan .slick_product_detail').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        pauseOnHover: true,
        arrows: true,
        // autoplay: true,
        responsive: [
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
            }
        },
        {
            breakpoint: 321,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
            }
        }
        ]        
    });

    $('.tintuc_lienquan .slick_news_detail').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        pauseOnHover: true,
        arrows: true,
        autoplay: false,
        responsive: [
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: false,
            }
        }
        ]        
    });

    $('.news_mobile .news_home_mobile').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        pauseOnHover: true,
        responsive: [
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        }
        ]        
    });

    $('.cart_single').click(function(){
        var id = $(this).attr('id-item');

        var cout = $(this).attr('cout-cart');
        
        $.ajax({
            type: 'POST',
            url:     ajax_obj.ajaxurl,
            data:    ({
                action  : 'myajax',
                id : id,
            }),
            success: function(data){
                if (data == 0) {
                    alert('Đã thêm vào giỏ hàng');

                    window.location.href = window.location.href;
                }
            }
        });
    });

    $('.main-menu').meanmenu({
        meanScreenWidth: "1024",
        meanMenuContainer: ".mobile-menu",
    });

    // $width_browser = $(window).width();
    // console.log($width_browser);
    // if($width_browser <= 1024) {
    //     $(window).on('scroll', function () {
    //         if ($(window).scrollTop() > 50) {
    //             $('.header').addClass('stick');
    //         } else {
    //             $('.header').removeClass('stick');
    //         }
    //     });
    // }

    //back to top
    $(window).on('scroll', function() {
      if ($(window).scrollTop() > 100) {
        $("#back-to-top").css("display", "block");
      } else {
        $("#back-to-top").css("display", "none");
      }
    });
    if ($('#back-to-top').length) {
      $("#back-to-top").on('click', function() {
        $('html, body').animate({
          scrollTop: $('html, body').offset().top,
        }, 1000);
      });
    }

    $('.video-section-homepage .slick_video').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow: '<span class="a-left control-c prev slick-prev"><b><i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev</b></span>',
        nextArrow: '<span class="a-right control-c next slick-next"><b>Next <i class="fa fa-angle-double-right" aria-hidden="true"></i></b></span>',
        responsive: [
        {
            breakpoint: 769,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
            }
        }
        ]        
    });
});

