<?php
/**
 * Plugin Name: Extends Product
 * Plugin URI: https://github.com/garungabc
 * Description: After installed the plugin, single product page with woocommerce will appear inputs to option value for feature product and customer can choose any product to show on fronend page of your website   
 * Version: 1.0.0
 * Author: Garung
 * Author URI: https://github.com/garungabc
 * License: GPLv2
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: Extend
 * Domain Path: /src/languages/
 */

define('THEME_NAME', 'EXTENDS');
define('PLUGIN_DIR', plugin_dir_path(__DIR__));
define('EP_PLUGIN_DIR', plugin_dir_path(__FILE__));

class ExtendsProduct
{
	const WP_REPO_REGEX = '|^http[s]?://wordpress\.org/(?:extend/)?plugins/|';

	const IS_URL_REGEX = '|^http[s]?://|';

	public function __construct()
	{
		 $this->load_acf();
		register_activation_hook( __FILE__, [$this, 'active_plugin'] );
		register_deactivation_hook( __FILE__, [$this, 'deactive_plugin'] );
	}

	public function active_plugin() {
		flush_rewrite_rules();
	}

	public function deactive_plugin() {
		flush_rewrite_rules();
	}


	//====================================================
	public function load_require_plugin() {
		$plugins = array(
			array(
	            'name'               => esc_html__('Advanced Custom Fields', THEME_NAME),
	            'slug'               => 'acf',
	            'source'             => 'https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.11.zip',
	            'required'           => true,
	            'external_url'       => '',
	        )
	    );
	    /**
	     * Array of configuration settings. Amend each line as needed.
	     * If you want the default strings to be available under your own theme domain,
	     * leave the strings uncommented.
	     * Some of the strings are added into a sprintf, so see the comments at the
	     * end of each line for what each argument will be.
	     */
	    // $config = array(
	    //     'default_path' => '',                      // Default absolute path to pre-packaged plugins.
	    //     'menu'         => 'install-require-plugins', // Menu slug.
	    //     'has_notices'  => true,                    // Show admin notices or not.
	    //     'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
	    //     'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
	    //     'is_automatic' => false,                   // Automatically activate plugins after installation or not.
	    //     'message'      => '',                      // Message to output right before the plugins table.
	    //     'strings'      => array(
	    //         'page_title'                      => esc_html__( 'Install Required Plugins', THEME_NAME ),
	    //         'menu_title'                      => esc_html__( 'Install Plugins', THEME_NAME ),
	    //         'installing'                      => esc_html__( 'Installing Plugin: %s', THEME_NAME ), // %s = plugin name.
	    //         'oops'                            => esc_html__( 'Something went wrong with the plugin API.', THEME_NAME ),
	    //         'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', THEME_NAME ),
	    //         'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', THEME_NAME ),
	    //         'return'                          => esc_html__( 'Return to Required Plugins Installer', THEME_NAME ),
	    //         'plugin_activated'                => esc_html__( 'Plugin activated successfully.', THEME_NAME ),
	    //         'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', THEME_NAME ), // %s = dashboard link.
	    //         'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
	    //     )
	    // );

		foreach ( $plugins as $plugin ) {
			call_user_func( [$this, 'register'] , $plugin );
		}

		// if ( ! empty( $config ) && is_array( $config ) ) {
		// 	call_user_func( [$this, 'config'] , $config );
		// }
	}
	public function register( $plugin ) {
		if ( empty( $plugin['slug'] ) || empty( $plugin['name'] ) ) {
			return;
		}

		if ( empty( $plugin['slug'] ) || ! is_string( $plugin['slug'] ) || isset( $this->plugins[ $plugin['slug'] ] ) ) {
			return;
		}

		$defaults = array(
			'name'               => '',      // String
			'slug'               => '',      // String
			'source'             => 'repo',  // String
			'required'           => false,   // Boolean
			'version'            => '',      // String
			'force_activation'   => false,   // Boolean
			'force_deactivation' => false,   // Boolean
			'external_url'       => '',      // String
			'is_callable'        => '',      // String|Array.
		);

		// Prepare the received data.
		$plugin = wp_parse_args( $plugin, $defaults );

		// Standardize the received slug.
		$plugin['slug'] = $this->sanitize_key( $plugin['slug'] );

		// Forgive users for using string versions of booleans or floats for version number.
		// $plugin['version']            = (string) $plugin['version'];
		$plugin['source']             = empty( $plugin['source'] ) ? 'repo' : $plugin['source'];
		// $plugin['required']           = TGMPA_Utils::validate_bool( $plugin['required'] );
		// $plugin['force_activation']   = TGMPA_Utils::validate_bool( $plugin['force_activation'] );
		// $plugin['force_deactivation'] = TGMPA_Utils::validate_bool( $plugin['force_deactivation'] );

		// Enrich the received data.
		$plugin['file_path']   = $this->_get_plugin_basename_from_slug( $plugin['slug'] );
		$plugin['source_type'] = $this->get_plugin_source_type( $plugin['source'] );

		// Set the class properties.
		$this->plugins[ $plugin['slug'] ]    = $plugin;
		$this->sort_order[ $plugin['slug'] ] = $plugin['name'];

		// Should we add the force activation hook ?
		if ( true === $plugin['force_activation'] ) {
			$this->has_forced_activation = true;
		}

		// Should we add the force deactivation hook ?
		if ( true === $plugin['force_deactivation'] ) {
			$this->has_forced_deactivation = true;
		}
	}

	public function sanitize_key( $key ) {
		$raw_key = $key;
		$key     = preg_replace( '`[^A-Za-z0-9_-]`', '', $key );

		/**
		 * Filter a sanitized key string.
		 *
		 * @since 2.5.0
		 *
		 * @param string $key     Sanitized key.
		 * @param string $raw_key The key prior to sanitization.
		 */
		return apply_filters( 'tgmpa_sanitize_key', $key, $raw_key );
	}

	protected function _get_plugin_basename_from_slug( $slug ) {
		$keys = array_keys( $this->get_plugins() );

		foreach ( $keys as $key ) {
			if ( preg_match( '|^' . $slug . '/|', $key ) ) {
				return $key;
			}
		}

		return $slug;
	}

	public function get_plugins( $plugin_folder = '' ) {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		return get_plugins( $plugin_folder );
	}

	protected function get_plugin_source_type( $source ) {
		if ( 'repo' === $source || preg_match( self::WP_REPO_REGEX, $source ) ) {
			return 'repo';
		} elseif ( preg_match( self::IS_URL_REGEX, $source ) ) {
			return 'external';
		} else {
			return 'bundled';
		}
	}

	/**
	 * Amend default configuration settings.
	 *
	 * @since 2.0.0
	 *
	 * @param array $config Array of config options to pass as class properties.
	 */
	public function config( $config ) {
		$keys = array(
			'id',
			'default_path',
			'has_notices',
			'dismissable',
			'dismiss_msg',
			'menu',
			'parent_slug',
			'capability',
			'is_automatic',
			'message',
			'strings',
		);

		foreach ( $keys as $key ) {
			if ( isset( $config[ $key ] ) ) {
				if ( is_array( $config[ $key ] ) ) {
					$keys = array_merge( $keys, $config[ $key ] );
				} else {
					$keys = $config[ $key ];
				}
			}
		}
	}
	//=================================================================
	public function load_acf() {
		if(file_exists(PLUGIN_DIR . '/advandced-custom-fields/acf.php')) {
			require_once(PLUGIN_DIR . '/advandced-custom-fields/acf.php');
		} 
		else {
			add_action( 'register_require_plugin', [$this, 'load_require_plugin'] );
			do_action( 'register_require_plugin' );
		}
	}

	public function load_class($class_name = '') {
		if($class_name == '' || trim($class_name) == '' || $class_name == null) {
			wp_die("Class doesn't exist !");
		}

		if(file_exists(EP_PLUGIN_DIR . '/src/class' . $class_name . '.php')) {
			require_once(EP_PLUGIN_DIR . '/src/class' . $class_name . '.php');
		}

		if( class_exists( $class_name )) {
            call_user_func( $class_name );
        }
	}
}

new ExtendsProduct();